package com.inventario.imobilizado.controller;

import com.inventario.imobilizado.repository.UserInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.inventario.imobilizado.model.User;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
public class LoginController {

    @Autowired
    private UserInterface userInterface;

    @PostMapping("/login")
    public ModelAndView loginSubmit(
    		@ModelAttribute("user") User user,
    		BindingResult bindingResult,
    		Model model) 
    {

        User usuarioBanco = userInterface.findByEmail(user.getEmail());

        if (user.getEmail() == "" && user.getSenha() == ""){
            ModelAndView modelAndView = new ModelAndView();

            return modelAndView;
        }
        if (usuarioBanco != null){
            if (usuarioBanco.getSenha().equals(user.getSenha())){
                ModelAndView modelAndView = new ModelAndView();
                modelAndView.setViewName("redirect:page/infoGeral");
                return modelAndView;
            }
            else if (usuarioBanco.getSenha() != user.getSenha()){
                model.addAttribute("error", "Senha Incorreta.");
                ModelAndView modelAndView = new ModelAndView();

                return modelAndView;
            }
            else {
                ModelAndView modelAndView = new ModelAndView();

                return modelAndView;
            }
        }
        else {
            model.addAttribute("error", "Usuário não encontrado.");
            ModelAndView modelAndView = new ModelAndView();

            return modelAndView;
        }
    }
}
